﻿using System;
using System.Linq;

namespace StringCalculator
{
    public class Calculator : ICalculator
    {
        public int Add(string input)
        {
            if (string.IsNullOrEmpty(input)) return 0;

            char[] separators;
            if (input.Length > 1 && input.Substring(0, 2) == "//")
            {
                separators = new char[] { ',', '\n', input[0], input[1], input[2] };
            }
            else
                separators = new char[] { ',', '\n' };

            var spl = input.Split(separators);
            int sum = input
                .Split(separators)
                .Where(s => !string.IsNullOrEmpty(s))
                .Select((s) =>
                {
                    int a = Int32.Parse(s);
                    if (a < 0) throw new ArgumentException("negative value");
                    return a;
                }
                )
                .Where(n => n < 1000)
                .Sum();
            return sum;
        }
    }
}
