using NUnit.Framework;
using StringCalculator;
using System;
using System.Text;

namespace Tests
{
    public class CalculatorTests
    {
        private Calculator calculator;
        private Random random;

        [SetUp]
        public void Setup()
        {
            calculator = new Calculator();
            random = new Random();
        }

        [Test]
        public void Add_StringIsEmpty_ShouldReturnZero()
        {
            int result = calculator.Add("");
            Assert.AreEqual(0, result);
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        public void Add_SingleValue_ShouldReturnTheValue(int value)
        {
            int result = calculator.Add(value.ToString());
            Assert.AreEqual(value, result);
        }

        [Test]
        [TestCase(1, 1)]
        [TestCase(10, 1)]
        public void Add_TwoValuesSeparatedByComma_ShouldReturnTheValue(int a, int b)
        {
            var input = a.ToString() + "," + b.ToString();
            int result = calculator.Add(input);
            Assert.AreEqual(a + b, result);
        }

        [Test]
        [TestCase(1, 1)]
        public void Add_TwoValuesSeparatedByNewLine_ShouldReturnTheValue(int a, int b)
        {
            var input = a.ToString() + "\n" + b.ToString();
            int result = calculator.Add(input);
            Assert.AreEqual(a + b, result);
        }

        private (string input, int sum) BuildInputAndSum(params int[] values)
        {
            var separators = new string[] { ",", "\n" };
            StringBuilder input = new StringBuilder();
            int sum = 0;
            for (int i = 0; i < values.Length - 1; ++i)
            {
                sum += values[i];
                input.Append(values[i].ToString() + separators[random.Next(0, separators.Length)]);
            }
            return (input.ToString(), sum);
        }

        private string BuildInput(params int[] values)
        {
            var separators = new string[] { ",", "\n" };
            StringBuilder input = new StringBuilder();

            for (int i = 0; i < values.Length - 1; ++i)
            {
                input.Append(values[i].ToString() + separators[random.Next(0, separators.Length)]);
            }
            return input.ToString();
        }

        [Test]
        [TestCase(1, 1, 2)]
        public void Add_ManyValuesSeparatedByNewLineOrComma_ShouldReturnTheValue(params int[] values)
        {

            (string input, int sum) = BuildInputAndSum(values);
            int result = calculator.Add(input);

            Assert.AreEqual(sum, result);
        }

        [Test]
        [TestCase(-1, 1, 2)]
        public void Add_NegativeValue_ShouldThrowException(params int[] values)
        {
            (string input, int sum) = BuildInputAndSum(values);

            Assert.Throws(typeof(ArgumentException), () => calculator.Add(input));
        }

        [Test]
        [TestCase(1, 1, 2000)]
        public void Add_OneValueGreaterThanThousand_ShouldIgnoreValueGreaterThanThousand(params int[] values)
        {
            string input = BuildInput(values);
            int result = calculator.Add(input);
            int sum = 0;
            foreach (int v in values)
                if (v < 1000) sum += v;

            Assert.AreEqual(sum, result);
        }

        [Test]
        public void Add_InputStartsDoubleSlashAndNewSeparator_ShouldReturnValue()
        {
            string input = "//#1,1#1";

            int result = calculator.Add(input);

            Assert.AreEqual(3, result);
        }
    }
}